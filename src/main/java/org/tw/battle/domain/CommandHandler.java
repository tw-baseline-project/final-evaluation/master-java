package org.tw.battle.domain;

/**
 * @author Liu Xia
 *
 * IMPORTANT: You cannot modify this file.
 */
public interface CommandHandler {
    /**
     * Determine if the `commandName` can be handled by this command handler.
     *
     * @param commandName The name of the command
     * @return `true` if the command can be handled. Otherwise, `false`.
     */
    boolean canHandle(String commandName);

    /**
     * Handle the command and return response.
     *
     * @param commandArgs The argument of the command. Please note that these
     *                    arguments contain no command name.
     * @return The response to this command.
     */
    CommandResponse handle(String[] commandArgs) throws Exception;
}
